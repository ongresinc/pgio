/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.base.Preconditions;
import com.google.common.primitives.UnsignedLong;

import java.time.Instant;

public class SystemIo {
  
  private final Instant timestamp;
  private final UnsignedLong pgpgin;
  private final UnsignedLong pgpgout;
  
  public SystemIo(Instant timestamp, UnsignedLong pgpgin, UnsignedLong pgpgout) {
    super();
    this.timestamp = timestamp;
    this.pgpgin = pgpgin;
    this.pgpgout = pgpgout;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public UnsignedLong getPgpgin() {
    return pgpgin;
  }

  public UnsignedLong getPgpgout() {
    return pgpgout;
  }
  
  @Override
  public String toString() {
    return "SystemIo [timestamp=" + timestamp + ", pgpgin=" + pgpgin + ", pgpgout=" + pgpgout + "]";
  }

  public static class Builder {
    private Instant timestamp = Instant.now();
    private UnsignedLong pgpgin;
    private UnsignedLong pgpgout;
    
    public Builder withPgpgin(String pgpgin) {
      this.pgpgin = UnsignedLong.valueOf(pgpgin);
      return this;
    }
    
    public Builder withPgpgout(String pgpgout) {
      this.pgpgout = UnsignedLong.valueOf(pgpgout);
      return this;
    }
    
    public SystemIo build() {
      Preconditions.checkArgument(pgpgin != null);
      Preconditions.checkArgument(pgpgout != null);
      
      return new SystemIo(timestamp, pgpgin, pgpgout);
    }
  }
}
