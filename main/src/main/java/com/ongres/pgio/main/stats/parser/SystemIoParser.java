/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats.parser;

import com.google.common.base.Charsets;
import com.ongres.pgio.main.stats.SystemIo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SystemIoParser {
  public SystemIoParser() {
  }

  public SystemIo parse() {
    SystemIo.Builder builder = new SystemIo.Builder();
    
    try (Scanner scanner = new Scanner(
        new FileInputStream("/proc/vmstat"), 
        Charsets.UTF_8.name())) {
      while (scanner.hasNext()) {
        String line = scanner.nextLine();
        if (line.startsWith("pgpgin ")) {
          builder.withPgpgin(line.substring("pgpgin ".length()));
          continue;
        }
        if (line.startsWith("pgpgout ")) {
          builder.withPgpgout(line.substring("pgpgout ".length()));
          continue;
        }
      }
    } catch (FileNotFoundException ex) {
      throw new RuntimeException(ex);
    }
    
    return builder.build();
  }
}
