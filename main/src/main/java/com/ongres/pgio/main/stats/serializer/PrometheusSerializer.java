/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats.serializer;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.UnsignedLong;
import com.ongres.pgio.main.stats.IoStat;

import java.io.PrintStream;
import java.util.Map;
import java.util.Optional;

public class PrometheusSerializer implements StatSerializer {

  private final PrintStream out;

  public PrometheusSerializer(PrintStream out) {
    this.out = out;
  }

  @Override
  public void serialize(IoStat stat) {
    for (Map.Entry<String, Optional<UnsignedLong>> entry : ImmutableMap.of(
        "rchar", stat.getRchar(),
        "wchar", stat.getWchar(),
        "read_bytes", stat.getReadBytes(),
        "write_bytes", stat.getWriteBytes(),
        "cancelled_write_bytes", stat.getCancelledWriteBytes()
        ).entrySet()) {
      String label = entry.getKey();
      Optional<UnsignedLong> value = entry.getValue();
      
      if (!value.isPresent()) {
        continue;
      }
      
      out.print("# HELP ");
      out.print(label);
      out.print(' ');
      out.println(label);
      out.print("# TYPE ");
      out.print(label);
      out.println(" gauge");
      out.print(label);
      out.print("{label=");
      out.print(protectString(stat.getLabel()));
      out.print(stringIfPresentOrEmpty(stat.getPid().map(pid -> " pid=" + pid)));
      out.print(stringIfPresentOrEmpty(stat.getPpid().map(ppid -> " ppid=" + ppid)));
      out.print("} ");
      out.print(value.get());
      out.print(' ');
      out.println(stat.getTimestamp().toEpochMilli());
    }
  }

  public String protectString(String value) {
    return '"' + value.replace("\\", "\\\\").replace("\n", "\\n").replace("\"", "\\\"") + '"';
  }

  public <T> String stringIfPresentOrEmpty(Optional<T> optional) {
    return optional.map(value -> String.valueOf(value)).orElse("");
  }
}
