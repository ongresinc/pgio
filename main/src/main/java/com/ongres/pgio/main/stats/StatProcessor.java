/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Streams;
import com.ongres.pgio.main.config.Config;
import com.ongres.pgio.main.stats.parser.ProcessInfoParser;
import com.ongres.pgio.main.stats.parser.ProcessIoParser;
import com.ongres.pgio.main.stats.parser.SystemIoParser;
import com.ongres.pgio.main.stats.serializer.StatSerializer;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StatProcessor {

  private final Config config;
  private final StatSerializer serializer;
  private final PrintStream err;
  private final SystemIoParser systemIoParser = new SystemIoParser();
  
  public StatProcessor(Config config, StatSerializer serializer, PrintStream err) {
    super();
    this.config = config;
    this.serializer = serializer;
    this.err = err;
  }
  
  public void begin() {
    serializer.beginSerialization();
  }
  
  public void end() {
    serializer.endSerialization();
  }
  
  public StatSnapshot getNext(Optional<StatSnapshot> previousStats) throws Exception {
    ImmutableMap<Integer, ProcessInfo> processes = reduceSubdirectory(Paths.get("/proc"), 
        stream -> reduceCurrentProcessInfoList(stream, previousStats), this::isPidDir);
    
    List<ProcessInfo> filteredProcessInfoList = processes
        .values()
        .stream()
        .filter(info -> config.getPpid()
            .map(ppid -> info.getPid() == ppid || info.isChildOf(ppid, processes))
            .orElse(true))
        .collect(Collectors.toList());
    
    ImmutableMap<Integer, ProcessStat> stats = filteredProcessInfoList
        .stream()
        .map(info -> getPreviousOrParseIo(previousStats, info))
        .filter(optionalStat -> optionalStat.isPresent())
        .map(optionalStat -> optionalStat.get())
        .collect(ImmutableMap.toImmutableMap(entry -> entry.getInfo().getPid(), entry -> entry));
    
    if (config.getProcessGroups().isPresent()) {
      Map<Integer, ProcessStat> currentStats = stats;
      Consumer<GroupStat> serializer;
      serializer = stat -> this.serializer.serialize(stat);
      Streams.concat(
          config.getProcessGroups().get().stream(), 
          Stream.of(new OtherGroupInfo(config.getProcessGroups().get())))
        .map(info -> extractGroupStat(info, currentStats))
        .forEach(stat -> serializer.accept(stat));
    } else {
      Consumer<ProcessStat> serializer;
      serializer = stat -> this.serializer.serialize(stat);

      stats
        .values()
        .forEach(stat -> serializer.accept(stat));
    }
    
    SystemIo systemIo = systemIoParser.parse();
    SystemStat systemStat = previousStats
        .map(stat -> stat.getSystemStat().next(systemIo))
        .orElseGet(() -> new SystemStat(systemIo));
    
    if (config.isShowSystem()) {
      serializer.serialize(systemStat);
    }
    
    if (config.isShowOther()) {
      ProcessStatAccumulator processStatAccumulator = stats
          .values()
          .stream()
          .reduce(new ProcessStatAccumulator(), 
              (result, stat) -> result.add(stat), (o, v) -> v);
      OtherStat otherStat = new OtherStat(systemStat, processStatAccumulator);
      serializer.serialize(otherStat);
    }
    
    return new StatSnapshot(processes, stats, systemStat);
  }

  private boolean isPidDir(Path path) {
    return IntStream.range(0, path.getFileName().toString().length())
        .allMatch(i -> path.getFileName().toString().charAt(i) >= '0' 
          && path.getFileName().toString().charAt(i) <= '9');
  }

  private ImmutableMap<Integer, ProcessInfo> reduceCurrentProcessInfoList(Stream<Path> porcsStream,
      Optional<StatSnapshot> previousStats) {
    return porcsStream
        .filter(path -> Files.isReadable(path))
        .filter(path -> Files.isDirectory(path))
        .map(path -> path.getFileName().toString())
        .map(name -> Integer.parseInt(name))
        .map(pid -> getPreviousOrParseInfo(previousStats, pid))
        .filter(optionalInfo -> optionalInfo.isPresent())
        .map(optionalInfo -> optionalInfo.get())
        .collect(ImmutableMap.toImmutableMap(info -> info.getPid(), info -> info));
  }
  
  private <T> T reduceSubdirectory(Path path, 
      Function<Stream<Path>, T> streamFunction, Filter<Path> filter) {
    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path, filter)) {
      return streamFunction.apply(StreamSupport
          .stream(directoryStream.spliterator(), false));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  private Optional<ProcessInfo> getPreviousOrParseInfo(
      Optional<StatSnapshot> previousStats, Integer pid) {
    if (previousStats.isPresent() && previousStats.get().getProcesses().containsKey(pid)) {
      return Optional.of(previousStats.get().getProcesses().get(pid));
    } else {
      return emptyIfException(() -> new ProcessInfoParser(pid).parse(), 
          () -> "Can not parse process info " + pid + ": ");
    }
  }

  private Optional<ProcessStat> getPreviousOrParseIo(
      Optional<StatSnapshot> previousStats, ProcessInfo info) {
    if (previousStats.isPresent() && previousStats.get().getStats().containsKey(info.getPid())) {
      return emptyIfException(
          () -> previousStats.get().getStats().get(info.getPid())
            .next(new ProcessIoParser(info.getPid()).parse()),
          () -> "Can not parse process io " + info.getPid() + ": ");
    } else {
      return emptyIfException(
          () -> new ProcessStat(info, new ProcessIoParser(info.getPid()).parse()),
          () -> "Can not parse process io " + info.getPid() + ": ");
    }
  }
  
  private <T> Optional<T> emptyIfException(
      Callable<T> callable, Supplier<String> errorPrefix) {
    try {
      return Optional.of(callable.call());
    } catch (Throwable throwable) {
      if (config.isDebug()) {
        err.println(errorPrefix.get() + throwable.getMessage());
        throwable.printStackTrace(err);
      }
      return Optional.empty();
    }
  }
  
  private GroupStat extractGroupStat(GroupInfo info, Map<Integer, ProcessStat> stats) {
    return new GroupStat(info, stats
        .values()
        .stream()
        .filter(stat -> info.belongsToGroup(stat.getInfo()))
        .reduce(new ProcessGroupStat.Builder(), (builder, stat) -> builder.add(stat), (u, v) -> v)
        .build());
  }
}
