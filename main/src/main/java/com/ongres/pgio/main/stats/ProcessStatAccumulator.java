/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.primitives.UnsignedLong;

public class ProcessStatAccumulator {
  private final UnsignedLong readBytes;
  private final UnsignedLong writeBytes;
  
  public ProcessStatAccumulator() {
    super();
    this.readBytes = UnsignedLong.ZERO;
    this.writeBytes = UnsignedLong.ZERO;
  }
  
  private ProcessStatAccumulator(UnsignedLong readBytes, UnsignedLong writeBytes) {
    super();
    this.readBytes = readBytes;
    this.writeBytes = writeBytes;
  }

  public UnsignedLong getReadBytes() {
    return readBytes;
  }

  public UnsignedLong getWriteBytes() {
    return writeBytes;
  }
  
  @Override
  public String toString() {
    return "ProcessStatAccumulator [readBytes=" + readBytes + ", writeBytes=" + writeBytes + "]";
  }

  public ProcessStatAccumulator add(ProcessStat processIo) {
    return new ProcessStatAccumulator(
        readBytes.plus(processIo.getReadBytes()
            .orElse(UnsignedLong.ZERO)), 
        writeBytes.plus(processIo.getWriteBytes()
            .orElse(UnsignedLong.ZERO)));
  }
}
