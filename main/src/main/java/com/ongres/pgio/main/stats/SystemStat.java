/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import static com.ongres.pgio.main.stats.StatUtil.difference;

import com.google.common.primitives.UnsignedLong;
import com.ongres.pgio.main.stats.serializer.StatUnit;

import java.time.Instant;
import java.util.Optional;

public class SystemStat implements IoStat {
  
  private final SystemIo currentIo;
  private final Optional<SystemIo> previousIo;
  
  public SystemStat(SystemIo currentIo) {
    super();
    this.currentIo = currentIo;
    this.previousIo = Optional.empty();
  }
  
  private SystemStat(SystemIo currentIo, Optional<SystemIo> previousIo) {
    super();
    this.currentIo = currentIo;
    this.previousIo = previousIo;
  }

  public SystemIo getCurrentIo() {
    return currentIo;
  }

  public Optional<SystemIo> getPreviousIo() {
    return previousIo;
  }

  public Instant getTimestamp() {
    return currentIo.getTimestamp();
  }

  public Optional<UnsignedLong> getPgpgin() {
    return previousIo
        .map(previousIo -> difference(currentIo.getPgpgin(), previousIo.getPgpgin()));
  }

  public Optional<UnsignedLong> getPgpgout() {
    return previousIo
        .map(previousIo -> difference(currentIo.getPgpgout(), previousIo.getPgpgout()));
  }

  @Override
  public String getLabel() {
    return "<system>";
  }

  @Override
  public Optional<Integer> getPid() {
    return Optional.empty();
  }

  @Override
  public Optional<Integer> getPpid() {
    return Optional.empty();
  }

  @Override
  public Optional<UnsignedLong> getRchar() {
    return Optional.empty();
  }

  @Override
  public Optional<UnsignedLong> getWchar() {
    return Optional.empty();
  }

  @Override
  public Optional<UnsignedLong> getReadBytes() {
    return getPgpgin().map(pgpgin -> pgpgin.times(StatUnit.KILOBYTE.getScale()));
  }

  @Override
  public Optional<UnsignedLong> getWriteBytes() {
    return getPgpgout().map(pgpgout -> pgpgout.times(StatUnit.KILOBYTE.getScale()));
  }

  @Override
  public Optional<UnsignedLong> getCancelledWriteBytes() {
    return Optional.empty();
  }

  public SystemStat next(SystemIo currentIo) {
    return new SystemStat(currentIo, Optional.of(this.currentIo));
  }

  @Override
  public String toString() {
    return "SystemStat [currentIo=" + currentIo + ", previousIo=" + previousIo + "]";
  }
}
