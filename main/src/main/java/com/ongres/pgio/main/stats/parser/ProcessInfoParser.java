/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats.parser;

import com.google.common.base.Charsets;
import com.ongres.pgio.main.stats.ProcessInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ProcessInfoParser {
  private final int pid;

  public ProcessInfoParser(int pid) {
    this.pid = pid;
  }

  public ProcessInfo parse() {
    ProcessInfo.Builder builder = new ProcessInfo.Builder(pid);
    
    try (Scanner scanner = new Scanner(
        new FileInputStream("/proc/" + pid + "/cmdline"), 
        Charsets.UTF_8.name())) {
      if (scanner.hasNext()) {
        String line = scanner.nextLine();
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < line.length(); index++) {
          if (line.charAt(index) == 0) {
            stringBuilder.append(" ");
          } else {
            stringBuilder.append(line.charAt(index));
          }
        }
        builder.withLabel(stringBuilder.toString());
      }
    } catch (FileNotFoundException ex) {
      throw new RuntimeException(ex);
    }
    
    try (Scanner scanner = new Scanner(
        new FileInputStream("/proc/" + pid + "/stat"), 
        Charsets.UTF_8.name())) {
      if (scanner.hasNext()) {
        String line = scanner.nextLine();
        int valueCount = 0;
        final int internalLabelCount = 2;
        final int ppidCount = 4;
        int valueStart = 0;
        int valueEnd = 0;
        boolean insideParenthesis = false;
        for (int index = 0; valueCount < ppidCount && index < line.length(); index++) {
          valueEnd = index;
          if (insideParenthesis) {
            if (line.charAt(index) == ')') {
              insideParenthesis = false;
            }
          } else {
            if (index > 0 && line.charAt(index - 1) == ' ') {
              valueStart = index;
            }
            switch (line.charAt(index)) {
              case ' ':
                valueCount++;
                switch (valueCount) {
                  case internalLabelCount:
                    if (builder.hasEmptyLabel()) {
                      builder.withLabel(line.substring(valueStart, valueEnd));
                    }
                    break;
                  case ppidCount:
                    builder.withPpid(Integer.parseInt(line.substring(valueStart, valueEnd)));
                    break;
                  default:
                    break;
                }
                break;
              case '(':
                insideParenthesis = true;
                break;
              default:
                break;
            }
          }
        }
      }
    } catch (FileNotFoundException ex) {
      throw new RuntimeException(ex);
    }
    
    return builder.build();
  }
}
