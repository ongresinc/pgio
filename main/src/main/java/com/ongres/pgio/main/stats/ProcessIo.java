/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.base.Preconditions;
import com.google.common.primitives.UnsignedLong;

import java.time.Instant;

public class ProcessIo {
  private final Instant timestamp;
  private final UnsignedLong rchar;
  private final UnsignedLong wchar;
  private final UnsignedLong readBytes;
  private final UnsignedLong writeBytes;
  private final UnsignedLong cancelledWriteBytes;
  
  private ProcessIo(Instant timestamp, UnsignedLong rchar, UnsignedLong wchar, 
      UnsignedLong readBytes, UnsignedLong writeBytes, UnsignedLong cancelledWriteBytes) {
    super();
    this.timestamp = timestamp;
    this.rchar = rchar;
    this.wchar = wchar;
    this.readBytes = readBytes;
    this.writeBytes = writeBytes;
    this.cancelledWriteBytes = cancelledWriteBytes;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public UnsignedLong getRchar() {
    return rchar;
  }

  public UnsignedLong getWchar() {
    return wchar;
  }

  public UnsignedLong getReadBytes() {
    return readBytes;
  }

  public UnsignedLong getWriteBytes() {
    return writeBytes;
  }

  public UnsignedLong getCancelledWriteBytes() {
    return cancelledWriteBytes;
  }
  
  @Override
  public String toString() {
    return "ProcessIo [timestamp=" + timestamp + ", rchar=" + rchar + ", wchar=" + wchar
        + ", readBytes=" + readBytes + ", writeBytes=" + writeBytes + ", cancelledWriteBytes="
        + cancelledWriteBytes + "]";
  }

  public static class Builder {
    private final Instant timestamp = Instant.now();
    private UnsignedLong rchar;
    private UnsignedLong wchar;
    private UnsignedLong readBytes;
    private UnsignedLong writeBytes;
    private UnsignedLong cancelledWriteBytes;
    
    public Builder withRchar(String rchar) {
      this.rchar = UnsignedLong.valueOf(rchar);
      return this;
    }
    
    public Builder withWchar(String wchar) {
      this.wchar = UnsignedLong.valueOf(wchar);
      return this;
    }
    
    public Builder withReadBytes(String readBytes) {
      this.readBytes = UnsignedLong.valueOf(readBytes);
      return this;
    }
    
    public Builder withWriteBytes(String writeBytes) {
      this.writeBytes = UnsignedLong.valueOf(writeBytes);
      return this;
    }
    
    public Builder withCancelledWriteBytes(String cancelledWriteBytes) {
      this.cancelledWriteBytes = UnsignedLong.valueOf(cancelledWriteBytes);
      return this;
    }
    
    public ProcessIo build() {
      Preconditions.checkArgument(rchar != null);
      Preconditions.checkArgument(wchar != null);
      Preconditions.checkArgument(readBytes != null);
      Preconditions.checkArgument(writeBytes != null);
      Preconditions.checkArgument(cancelledWriteBytes != null);
      
      return new ProcessIo(timestamp, rchar, wchar, readBytes, writeBytes, cancelledWriteBytes);
    }
  }
}
