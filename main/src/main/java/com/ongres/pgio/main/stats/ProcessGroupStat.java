/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.collect.ImmutableList;
import com.google.common.primitives.UnsignedLong;

import java.time.Instant;
import java.util.Optional;
import java.util.function.Function;

public class ProcessGroupStat {
  private final Instant timestamp;
  private final ImmutableList<ProcessStat> stats;
  
  private ProcessGroupStat(Instant timestamp, ImmutableList<ProcessStat> stats) {
    super();
    this.timestamp = timestamp;
    this.stats = stats;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public Optional<UnsignedLong> getRchar() {
    return reduceFromStats(stat -> stat.getRchar());
  }

  public Optional<UnsignedLong> getWchar() {
    return reduceFromStats(stat -> stat.getWchar());
  }

  public Optional<UnsignedLong> getReadBytes() {
    return reduceFromStats(stat -> stat.getReadBytes());
  }

  public Optional<UnsignedLong> getWriteBytes() {
    return reduceFromStats(stat -> stat.getWriteBytes());
  }

  public Optional<UnsignedLong> getCancelledWriteBytes() {
    return reduceFromStats(stat -> stat.getCancelledWriteBytes());
  }
  
  private Optional<UnsignedLong> reduceFromStats(
      Function<ProcessStat, Optional<UnsignedLong>> valueSupplier) {
    return Optional.of(stats.stream()
        .reduce(Optional.<UnsignedLong>empty(), 
            (result, stat) -> valueSupplier.apply(stat)
            .map(value -> result.orElse(UnsignedLong.ZERO).plus(value)), 
            (u, v) -> v)
        .orElse(UnsignedLong.ZERO));
  }
  
  @Override
  public String toString() {
    return "ProcessGroupIo [timestamp=" + timestamp + ", stats=" + stats + "]";
  }

  public static class Builder {
    private Optional<Instant> timestamp = Optional.empty();
    private ImmutableList.Builder<ProcessStat> processes = ImmutableList.builder();
    
    public Builder add(ProcessStat processIo) {
      timestamp = timestamp
          .filter(timestamp -> timestamp.isAfter(processIo.getTimestamp()))
          .map(timestamp -> Optional.of(timestamp))
          .orElse(Optional.of(processIo.getTimestamp()));
      processes.add(processIo);
      return this;
    }
    
    public ProcessGroupStat build() {
      return new ProcessGroupStat(timestamp.orElse(Instant.now()), processes.build());
    }
  }
}
