/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats.serializer;

import com.ongres.pgio.main.config.Config;
import com.ongres.pgio.main.stats.IoStat;

import java.io.PrintStream;
import java.util.Optional;

public class CsvSerializer implements StatSerializer {

  private final Config config;
  private final PrintStream out;

  public CsvSerializer(Config config, PrintStream out) {
    this.config = config;
    this.out = out;
  }

  @Override
  public void beginSerialization() {
    if (!config.isNoPrintHeader()) {
      out.println("timestamp,pid,ppid,label,rchar,wchar,"
          + "read_bytes,write_bytes,cancelled_write_bytes");
    }
  }

  @Override
  public void serialize(IoStat stat) {
    out.print(stat.getTimestamp());
    out.print(',');
    out.print(protectString(stat.getLabel()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getPid()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getPpid()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getRchar()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getWchar()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getReadBytes()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getWriteBytes()));
    out.print(',');
    out.print(stringIfPresentOrEmpty(stat.getCancelledWriteBytes()));
    out.println();
  }

  public String protectString(String value) {
    return '"' + value.replace("\"", "\"\"") + '"';
  }

  public <T> String stringIfPresentOrEmpty(Optional<T> optional) {
    return optional.map(value -> String.valueOf(value)).orElse("");
  }
}
