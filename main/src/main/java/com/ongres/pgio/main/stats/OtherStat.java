/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.primitives.UnsignedLong;

import java.time.Instant;
import java.util.Optional;

public class OtherStat implements IoStat {

  private final SystemStat systemStat;
  private final ProcessStatAccumulator processStatAccumulator;

  public OtherStat(SystemStat systemStat, ProcessStatAccumulator processStatAccumulator) {
    super();
    this.systemStat = systemStat;
    this.processStatAccumulator = processStatAccumulator;
  }

  @Override
  public Instant getTimestamp() {
    return systemStat.getTimestamp();
  }

  @Override
  public String getLabel() {
    return "<other>";
  }

  @Override
  public Optional<Integer> getPid() {
    return Optional.empty();
  }

  @Override
  public Optional<Integer> getPpid() {
    return Optional.empty();
  }

  @Override
  public Optional<UnsignedLong> getRchar() {
    return Optional.empty();
  }

  @Override
  public Optional<UnsignedLong> getWchar() {
    return Optional.empty();
  }

  @Override
  public Optional<UnsignedLong> getReadBytes() {
    return systemStat.getReadBytes().map(readBytes -> StatUtil
        .subtract(readBytes, processStatAccumulator.getReadBytes()));
  }

  @Override
  public Optional<UnsignedLong> getWriteBytes() {
    return systemStat.getWriteBytes().map(writeBytes -> StatUtil
        .subtract(writeBytes, processStatAccumulator.getWriteBytes()));
  }

  @Override
  public Optional<UnsignedLong> getCancelledWriteBytes() {
    return Optional.empty();
  }
}
