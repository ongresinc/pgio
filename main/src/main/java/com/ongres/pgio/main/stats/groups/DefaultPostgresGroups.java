/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats.groups;

import com.google.common.collect.ImmutableList;
import com.ongres.pgio.main.stats.ProcessGroupInfo;

public class DefaultPostgresGroups {
  public static final ImmutableList<ProcessGroupInfo> GROUPS = ImmutableList.of(
      new ProcessGroupInfo.Builder("archiver")
        .addPattern(".*archiver.*").build(),
      new ProcessGroupInfo.Builder("wal sender")
        .addPattern(".*wal sender.*").build(),
      new ProcessGroupInfo.Builder("bgwriter")
        .addPattern(".*writer process.*").build(),
      new ProcessGroupInfo.Builder("autovacuum")
        .addPattern(".*autovacuum worker process.*").build(),
      new ProcessGroupInfo.Builder("stats")
        .addPattern(".*stats collector process.*").build(),
      new ProcessGroupInfo.Builder("wal writer")
        .addPattern(".*wal writer process.*").build(),
      new ProcessGroupInfo.Builder("checkpoint")
        .addPattern(".*checkpointer process.*").build());

  private DefaultPostgresGroups() {
    throw new UnsupportedOperationException();
  }
}
