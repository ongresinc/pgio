/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import static com.ongres.pgio.main.stats.StatUtil.difference;

import com.google.common.primitives.UnsignedLong;

import java.time.Instant;
import java.util.Optional;

public class ProcessStat implements IoStat {
  
  private final ProcessInfo info;
  private final ProcessIo currentIo;
  private final Optional<ProcessIo> previousIo;
  
  public ProcessStat(ProcessInfo info, ProcessIo currentIo) {
    super();
    this.info = info;
    this.currentIo = currentIo;
    this.previousIo = Optional.empty();
  }
  
  private ProcessStat(ProcessInfo info, ProcessIo currentIo, Optional<ProcessIo> previousIo) {
    super();
    this.info = info;
    this.currentIo = currentIo;
    this.previousIo = previousIo;
  }

  public ProcessInfo getInfo() {
    return info;
  }

  public ProcessIo getCurrentIo() {
    return currentIo;
  }

  public Optional<ProcessIo> getPreviousIo() {
    return previousIo;
  }

  @Override
  public String getLabel() {
    return info.getLabel();
  }

  @Override
  public Optional<Integer> getPid() {
    return Optional.of(info.getPid());
  }

  @Override
  public Optional<Integer> getPpid() {
    return info.getPpid();
  }

  @Override
  public Instant getTimestamp() {
    return currentIo.getTimestamp();
  }

  @Override
  public Optional<UnsignedLong> getRchar() {
    return previousIo
        .map(previousIo -> difference(currentIo.getRchar(), previousIo.getRchar()));
  }

  @Override
  public Optional<UnsignedLong> getWchar() {
    return previousIo
        .map(previousIo -> difference(currentIo.getWchar(), previousIo.getWchar()));
  }

  @Override
  public Optional<UnsignedLong> getReadBytes() {
    return previousIo
        .map(previousIo -> difference(currentIo.getReadBytes(), previousIo.getReadBytes()));
  }

  @Override
  public Optional<UnsignedLong> getWriteBytes() {
    return previousIo
        .map(previousIo -> difference(currentIo.getWriteBytes(), previousIo.getWriteBytes()));
  }

  @Override
  public Optional<UnsignedLong> getCancelledWriteBytes() {
    return previousIo
        .map(previousIo -> difference(currentIo.getCancelledWriteBytes(), 
            previousIo.getCancelledWriteBytes()));
  }

  public ProcessStat next(ProcessIo currentIo) {
    return new ProcessStat(info, currentIo, Optional.of(this.currentIo));
  }

  @Override
  public String toString() {
    return "ProcessStat [info=" + info + ", currentIo=" + currentIo + ", previousIo=" + previousIo
        + "]";
  }
}
