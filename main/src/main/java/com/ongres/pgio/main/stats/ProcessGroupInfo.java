/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.collect.ImmutableList;

import java.util.regex.Pattern;

public class ProcessGroupInfo implements GroupInfo {
  private final String name;
  private final ImmutableList<Pattern> groupPatterns;
  
  private ProcessGroupInfo(String name, ImmutableList<Pattern> groupPattern) {
    super();
    this.name = name;
    this.groupPatterns = groupPattern;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public boolean belongsToGroup(ProcessInfo info) {
    return groupPatterns
        .stream()
        .anyMatch(pattern -> pattern.matcher(info.getLabel()).matches());
  }

  @Override
  public String toString() {
    return "ProcessGroupInfo [name=" + name + ", groupPatterns=" + groupPatterns + "]";
  }

  public static class Builder {
    private final String name;
    private final ImmutableList.Builder<Pattern> groupPatterns = ImmutableList.builder();
    
    public Builder(String name) {
      this.name = name;
    }
    
    public Builder addPattern(String pattern) {
      groupPatterns.add(Pattern.compile(pattern));
      return this;
    }
    
    public ProcessGroupInfo build() {
      return new ProcessGroupInfo(name, groupPatterns.build());
    }
  }
}
