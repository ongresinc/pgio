/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats.parser;

import com.google.common.base.Charsets;
import com.ongres.pgio.main.stats.ProcessIo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ProcessIoParser {
  private final int pid;

  public ProcessIoParser(int pid) {
    this.pid = pid;
  }

  public ProcessIo parse() {
    ProcessIo.Builder builder = new ProcessIo.Builder();
    
    try (Scanner scanner = new Scanner(
        new FileInputStream("/proc/" + pid + "/io"), 
        Charsets.UTF_8.name())) {
      while (scanner.hasNext()) {
        String line = scanner.nextLine();
        if (line.startsWith("rchar: ")) {
          builder.withRchar(line.substring("rchar: ".length()));
          continue;
        }
        if (line.startsWith("wchar: ")) {
          builder.withWchar(line.substring("wchar: ".length()));
          continue;
        }
        if (line.startsWith("read_bytes: ")) {
          builder.withReadBytes(line.substring("read_bytes: ".length()));
          continue;
        }
        if (line.startsWith("write_bytes: ")) {
          builder.withWriteBytes(line.substring("write_bytes: ".length()));
          continue;
        }
        if (line.startsWith("cancelled_write_bytes: ")) {
          builder.withCancelledWriteBytes(line.substring("cancelled_write_bytes: ".length()));
          continue;
        }
      }
    } catch (FileNotFoundException ex) {
      throw new RuntimeException(ex);
    }
    
    return builder.build();
  }
}
