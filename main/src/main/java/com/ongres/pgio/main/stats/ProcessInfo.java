/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.base.Preconditions;

import java.util.Map;
import java.util.Optional;

public class ProcessInfo {
  private final int pid;
  private final Optional<Integer> ppid;
  private final String label;
  
  private ProcessInfo(int pid, Optional<Integer> ppid, String label) {
    this.ppid = ppid;
    this.pid = pid;
    this.label = label;
  }
  
  public int getPid() {
    return pid;
  }
  
  public Optional<Integer> getPpid() {
    return ppid;
  }
  
  public boolean isChildOf(int ppid, Map<Integer, ProcessInfo> processInfoMap) {
    if (isDirectChildOf(ppid)) {
      return true;
    }
    
    if (this.ppid.isPresent()) {
      int myPpid = this.ppid.get();
      
      if (processInfoMap.containsKey(myPpid)) {
        return processInfoMap.get(myPpid).isChildOf(ppid, processInfoMap);
      }
    }
    
    return false;
  }
  
  public boolean isDirectChildOf(int ppid) {
    return this.ppid.map(myPpid -> myPpid.intValue() == ppid).orElse(false);
  }
  
  public String getLabel() {
    return label;
  }
  
  @Override
  public String toString() {
    return "ProcessInfo [pid=" + pid + ", ppid=" + ppid + ", label=" + label + "]";
  }

  public static class Builder {
    private final int pid;
    private Optional<Integer> ppid = Optional.empty();
    private String label;
    
    public Builder(int pid) {
      this.pid = pid;
    }
    
    public Builder withPpid(int ppid) {
      this.ppid = Optional.of(ppid);
      return this;
    }
    
    public Builder withLabel(String label) {
      this.label = label;
      return this;
    }
    
    public boolean hasEmptyLabel() {
      return label == null || label.isEmpty();
    }
    
    public ProcessInfo build() {
      Preconditions.checkArgument(label != null);
      
      return new ProcessInfo(pid, ppid, label);
    }
  }
}
