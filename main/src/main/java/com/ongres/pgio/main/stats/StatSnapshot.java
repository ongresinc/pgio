/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.collect.ImmutableMap;

public class StatSnapshot {

  private final ImmutableMap<Integer, ProcessInfo> processes;
  private final ImmutableMap<Integer, ProcessStat> stats;
  private final SystemStat systemStat;

  public StatSnapshot(ImmutableMap<Integer, ProcessInfo> processes, 
      ImmutableMap<Integer, ProcessStat> stats, SystemStat systemStat) {
    super();
    this.processes = processes;
    this.stats = stats;
    this.systemStat = systemStat;
  }

  public ImmutableMap<Integer, ProcessInfo> getProcesses() {
    return processes;
  }

  public ImmutableMap<Integer, ProcessStat> getStats() {
    return stats;
  }

  public SystemStat getSystemStat() {
    return systemStat;
  }
}
