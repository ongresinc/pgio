/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.primitives.UnsignedLong;
import com.ongres.pgio.main.stats.serializer.StatSerializer;

import java.time.Instant;
import java.util.Optional;

public interface IoStat {
  public Instant getTimestamp();
  
  public String getLabel();
  
  public Optional<Integer> getPid();
  
  public Optional<Integer> getPpid();
  
  public Optional<UnsignedLong> getRchar();
  
  public Optional<UnsignedLong> getWchar();
  
  public Optional<UnsignedLong> getReadBytes();
  
  public Optional<UnsignedLong> getWriteBytes();
  
  public Optional<UnsignedLong> getCancelledWriteBytes();

  public default void serialize(StatSerializer serializer) {
    serializer.serialize(this);
  }
}
