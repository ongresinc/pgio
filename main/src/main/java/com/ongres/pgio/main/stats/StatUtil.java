/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main.stats;

import com.google.common.primitives.UnsignedLong;

public class StatUtil {

  public StatUtil() {
    throw new UnsupportedOperationException();
  }

  public static UnsignedLong difference(UnsignedLong left, UnsignedLong right) {
    int compare = left.compareTo(right);
    if (compare == 0) {
      return UnsignedLong.ZERO;
    } else if (compare > 0) {
      return left.minus(right);
    } else {
      return left.plus(UnsignedLong.MAX_VALUE.minus(right));
    }
  }
  
  public static UnsignedLong subtract(UnsignedLong left, UnsignedLong right) {
    if (left.compareTo(right) > 0) {
      return left.minus(right);
    }
    
    return UnsignedLong.ZERO;
  }
}
