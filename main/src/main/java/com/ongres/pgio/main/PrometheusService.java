/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.ongres.pgio.main;

import com.ongres.pgio.main.config.Config;
import com.ongres.pgio.main.stats.StatProcessor;
import com.ongres.pgio.main.stats.StatSnapshot;
import com.ongres.pgio.main.stats.serializer.PrometheusSerializer;
import com.ongres.pgio.main.stats.serializer.StatSerializer;
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.Response.Status;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

public class PrometheusService extends NanoHTTPD {
  private final Config config;
  private final AtomicReference<Optional<StatSnapshot>> previousStatsReference = 
      new AtomicReference<Optional<StatSnapshot>>(Optional.empty());
  
  public PrometheusService(Config config) {
    super(config.getPrometheusBind().getHostName(),
        config.getPrometheusPort());
    this.config = config;
  }

  @Override
  public Response serve(IHTTPSession session) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    PrintStream out = new PrintStream(byteArrayOutputStream);
    StatSerializer serializer = new PrometheusSerializer(out);
    StatProcessor statProcessor = new StatProcessor(config, serializer, System.err);
    previousStatsReference.getAndUpdate(
        previousStats -> emptyIfException(() -> statProcessor.getNext(previousStats)));
    return newFixedLengthResponse(Status.OK, NanoHTTPD.MIME_PLAINTEXT, 
        new ByteArrayInputStream(byteArrayOutputStream.toByteArray()), 
        byteArrayOutputStream.size());
  }

  private static <T> Optional<T> emptyIfException(Callable<T> call) {
    try {
      return Optional.of(call.call());
    } catch (Exception ex) {
      ex.printStackTrace(System.err);
      return Optional.empty();
    }
  }
}
