/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.ongres.pgio.main;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.ongres.pgio.main.config.Config;
import com.ongres.pgio.main.stats.ProcessGroupInfo;
import com.ongres.pgio.main.stats.StatProcessor;
import com.ongres.pgio.main.stats.StatSnapshot;
import com.ongres.pgio.main.stats.serializer.CsvSerializer;
import com.ongres.pgio.main.stats.serializer.StatSerializer;
import com.ongres.pgio.main.version.Version;

import fi.iki.elonen.NanoHTTPD;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

public class Main {
  public static void main(String[] args) throws Exception {
    System.exit(run(args));
  }
  
  private static int run(String[] args) throws Exception {
    OptionParser parser;
    OptionSet options;
    try {
      parser = createOptionParser();
      options = parser.parse(args);
    } catch (OptionException ex) {
      System.err.println(messageOrType(ex));
      return -1;
    } catch (RuntimeException ex) {
      onException(ex, true);
      return -1;
    } catch (Exception ex) {
      onException(ex, true);
      return -1;
    }
    
    try {
      if (options.has("help")) {
        parser.printHelpOn(System.out);
        return 0;
      }
      
      if (options.has("version")) {
        System.out.println(Version.getVersion());
        return 0;
      }
      
      Config config = configOptionSet(options);
      
      if (config.isPrometheusService()) {
        runPrometheusService(config);
      } else {
        runCollector(config);
      }
    } catch (RuntimeException ex) {
      return onException(ex, options.has("debug"));
    } catch (Exception ex) {
      return onException(ex, options.has("debug"));
    }
    
    return 0;
  }

  private static int onException(Exception ex, boolean debug) {
    System.err.println(messageOrType(ex));
    
    if (debug) {
      onExceptionHint(ex);
    }
    
    return -1;
  }

  private static void onExceptionHint(Exception ex) {
    System.err.println();
    System.err.println();
    System.err.println("If you think you hit a bug. Please open an issue attaching"
        + " the following stack trace at https://gitlab.com/ongresinc/pgio/issues/new");
    System.err.println();
    ex.printStackTrace(System.err);
    System.err.println();
    System.err.println("Try \"pgio --help\" for more information.");
    System.err.println();
  }

  private static String messageOrType(Exception ex) {
    String message = ex.getMessage();
    
    if (message == null) {
      return ex.getClass().getName();
    }
    
    return message;
  }
  
  private static void runPrometheusService(Config config) throws Exception {
    PrometheusService service = new PrometheusService(config);
    service.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
    while (true) {
      TimeUnit.MICROSECONDS.sleep(20);
    }
  }

  private static void runCollector(Config config) throws Exception, InterruptedException {
    StatSerializer serializer = new CsvSerializer(config, System.out);
    StatProcessor statProcessor = new StatProcessor(config, serializer, System.err);
    try {
      statProcessor.begin();
      Instant start = Instant.now();
      Optional<StatSnapshot> previousStats = Optional.empty();
      while (true) {
        previousStats = Optional.of(statProcessor.getNext(previousStats));
        if (config.getInterval() > 0) {
          Thread.sleep(config.getInterval() - Duration.between(
              start, Instant.now()).toMillis() % config.getInterval());
        }
      }
    } finally {
      statProcessor.end();
    }
  }

  private static OptionParser createOptionParser() {
    OptionParser parser = new OptionParser(false);
    parser.acceptsAll(Lists.newArrayList("h", "help"), 
        "Displays this help message and quit")
      .isForHelp();
    parser.acceptsAll(Lists.newArrayList("v", "version"), 
        "Show version and quit");
    parser.acceptsAll(Lists.newArrayList("D"), 
        "Specifies the file system location of the database configuration files."
        + " If this is omitted, the environment variable PGDATA is used.")
      .withRequiredArg();
    parser.acceptsAll(Lists.newArrayList("i", "interval"), 
        "Interval in milliseconds to gather stats")
      .withRequiredArg()
      .defaultsTo("3000");
    parser.acceptsAll(Lists.newArrayList("d", "debug"), 
        "Show debug messages");
    parser.acceptsAll(Lists.newArrayList("s", "show-system"), 
        "Print read/write data for the whole system");
    parser.acceptsAll(Lists.newArrayList("o", "show-other"), 
        "Print read/write data not accounted by any listed process");
    parser.acceptsAll(Lists.newArrayList("no-print-header"), 
        "Suppress print of CSV header");
    parser.acceptsAll(Lists.newArrayList("a", "advanced"), 
        "Enable advanced options");
    parser.acceptsAll(Lists.newArrayList("prometheus-service"), 
        "Run as a prometheus service (advanced option)")
      .availableIf("advanced");
    parser.acceptsAll(Lists.newArrayList("prometheus-bind"), 
        "The bind address of prometheus service, default is localhost (advanced option)")
      .availableIf("advanced")
      .withRequiredArg();
    parser.acceptsAll(Lists.newArrayList("prometheus-port"), 
        "The port of prometheus service, default is 9544 (advanced option)")
      .availableIf("advanced")
      .withRequiredArg();
    parser.acceptsAll(Lists.newArrayList("ppid"), 
        "Parent pid of the process to scan (advanced option)")
      .availableIf("advanced")
      .withRequiredArg();
    parser.acceptsAll(Lists.newArrayList("g", "group"), 
        "Group results using specified group configuration file (advanced option)")
      .availableIf("advanced")
      .withRequiredArg();
    return parser;
  }

  private static Config configOptionSet(OptionSet options) throws Exception {
    Config.Builder configBuilder = new Config.Builder();
    ConfigHelper configHelper = new ConfigHelper(options);
    if (System.getenv("PGDATA") != null) {
      configBuilder.withDataDir(Paths.get(System.getenv("PGDATA")));
    }
    configHelper.set("D", Paths::get, configBuilder::withDataDir);
    configHelper.set("interval", Long::valueOf, configBuilder::withInterval);
    configHelper.set("ppid", Integer::valueOf, configBuilder::withPpid);
    configHelper.setIf("no-print-header", configBuilder::withNoPrintHeader);
    configHelper.setIf("show-system", configBuilder::withShowSystem);
    configHelper.setIf("show-other", configBuilder::withShowOther);
    configHelper.set("group", Main::readGroupConfig, configBuilder::appendProcessGroups);
    configHelper.setIf("prometheus-service", configBuilder::withPrometheusService);
    configHelper.set("prometheus-bind", Main::readInetAddress, configBuilder::withPrometheusBind);
    configHelper.set("prometheus-port", Integer::valueOf, configBuilder::withPrometheusPort);
    configHelper.setIf("debug", configBuilder::withDebug);
    
    Config config = configBuilder.build();
    
    if (!config.getPpid().isPresent()) {
      int postgresqlPid = parsePostgresPidFile(configBuilder.build().getDataDir());
      configBuilder.withPpid(postgresqlPid);
    }
    
    return configBuilder.build();
  }
  
  private static ImmutableList<ProcessGroupInfo> readGroupConfig(String config) {
    File groupConfigFile = new File(config);
    try (InputStream inputStream = new FileInputStream(groupConfigFile)) {
      ImmutableList.Builder<ProcessGroupInfo> builder = ImmutableList.builder();
      JsonValue groups = Json.createReader(inputStream).readValue();
      
      if (groups.getValueType() == ValueType.ARRAY) {
        groups.asJsonArray().forEach(element -> {
          if (element.getValueType() == ValueType.OBJECT) {
            appendFromObject(builder, element.asJsonObject());
          }
        });
      } else if (groups.getValueType() == ValueType.OBJECT) {
        appendFromObject(builder, groups.asJsonObject());
      }
      
      return builder.build();
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  private static void appendFromObject(ImmutableList.Builder<ProcessGroupInfo> builder,
      JsonObject groups) {
    groups.asJsonObject().forEach((key, value) -> {
      ProcessGroupInfo.Builder groupBuilder = new ProcessGroupInfo.Builder(key);
      value.asJsonArray()
        .forEach(entry -> groupBuilder.addPattern(((JsonString) entry).getString()));
      builder.add(groupBuilder.build());
    });
  }
  
  private static InetAddress readInetAddress(String address) {
    try {
      return InetAddress.getByName(address);
    } catch (UnknownHostException ex) {
      throw new RuntimeException(ex);
    }
  }
  
  private static int parsePostgresPidFile(Path dataDir) {
    Path postmasterPid = dataDir.resolve("postmaster.pid");
    
    try (Scanner scanner = new Scanner(
        new FileInputStream(postmasterPid.toFile()), 
        Charsets.UTF_8.name())) {
      if (scanner.hasNext()) {
        String line = scanner.nextLine();
        return Integer.parseInt(line);
      }
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    
    throw new RuntimeException("Error reading pid from file " + postmasterPid);
  }
  
  private static class ConfigHelper {
    public final OptionSet options;
    
    public ConfigHelper(OptionSet options) {
      this.options = options;
    }
    
    public <T> void set(String option, Function<String, T> mapValue, Consumer<T> setValue) {
      if (options.valueOf(option) != null) {
        setValue.accept(mapValue.apply((String) options.valueOf(option)));
      }
    }
    
    public void setIf(String option, Consumer<Boolean> setValue) {
      setValue.accept(options.has(option));
    }
  }
}
