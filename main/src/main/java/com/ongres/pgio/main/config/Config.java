/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.ongres.pgio.main.config;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.ongres.pgio.main.stats.ProcessGroupInfo;
import com.ongres.pgio.main.stats.groups.DefaultPostgresGroups;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.Optional;

public class Config {

  private final Path dataDir;
  private final long interval;
  private final Optional<Integer> ppid;
  private final boolean prometheusService;
  private final InetAddress prometheusBind;
  private final int prometheusPort;
  private final boolean noPrintHeader;
  private final boolean showSystem;
  private final boolean showOther;
  private final Optional<ImmutableList<ProcessGroupInfo>> processGroups;
  private final boolean debug;

  private Config(Path dataDir, long interval, Optional<Integer> ppid, 
      boolean prometheusService, InetAddress prometheusBind, int prometheusPort,
      boolean noPrintHeader, boolean showSystem, boolean showOther,
      Optional<ImmutableList<ProcessGroupInfo>> processGroups,
      boolean debug) {
    super();
    this.dataDir = dataDir;
    this.interval = interval;
    this.ppid = ppid;
    this.prometheusService = prometheusService;
    this.prometheusBind = prometheusBind;
    this.prometheusPort = prometheusPort;
    this.noPrintHeader = noPrintHeader;
    this.showSystem = showSystem;
    this.showOther = showOther;
    this.processGroups = processGroups;
    this.debug = debug;
  }

  public Path getDataDir() {
    return dataDir;
  }

  public long getInterval() {
    return interval;
  }

  public Optional<Integer> getPpid() {
    return ppid;
  }

  public boolean isPrometheusService() {
    return prometheusService;
  }

  public InetAddress getPrometheusBind() {
    return prometheusBind;
  }

  public int getPrometheusPort() {
    return prometheusPort;
  }

  public boolean isNoPrintHeader() {
    return noPrintHeader;
  }

  public boolean isShowOther() {
    return showOther;
  }

  public boolean isShowSystem() {
    return showSystem;
  }

  public Optional<ImmutableList<ProcessGroupInfo>> getProcessGroups() {
    return processGroups;
  }

  public boolean isDebug() {
    return debug;
  }

  public static class Builder {
    private Path dataDir;
    private long interval;
    private Optional<Integer> ppid = Optional.empty();
    private boolean prometheusService;
    private InetAddress prometheusBind;
    private int prometheusPort = 9544;
    private boolean noPrintHeader;
    private boolean showSystem;
    private boolean showOther;
    private Optional<ImmutableList<ProcessGroupInfo>> processGroups = 
        Optional.of(DefaultPostgresGroups.GROUPS);
    private boolean debug;
    
    public Builder() {
      try {
        prometheusBind = InetAddress.getByName("localhost");
      } catch (UnknownHostException ex) {
        throw new RuntimeException(ex);
      }
    }
    
    public Builder withDataDir(Path dataDir) {
      this.dataDir = dataDir;
      return this;
    }
    
    public Builder withInterval(long interval) {
      this.interval = interval;
      return this;
    }
    
    public Builder withPpid(Integer ppid) {
      this.ppid = Optional.ofNullable(ppid);
      return this;
    }
    
    public Builder withPrometheusService(boolean prometheusService) {
      this.prometheusService = prometheusService;
      return this;
    }
    
    public Builder withPrometheusBind(InetAddress prometheusBind) {
      this.prometheusBind = prometheusBind;
      return this;
    }
    
    public Builder withPrometheusPort(int prometheusPort) {
      this.prometheusPort = prometheusPort;
      return this;
    }
    
    public Builder withNoPrintHeader(boolean noPrintHeader) {
      this.noPrintHeader = noPrintHeader;
      return this;
    }
    
    public Builder withShowSystem(boolean showSystem) {
      this.showSystem = showSystem;
      return this;
    }
    
    public Builder withShowOther(boolean showOther) {
      this.showOther = showOther;
      return this;
    }
    
    public Builder withProcessGroups(ImmutableList<ProcessGroupInfo> processGroups) {
      this.processGroups = Optional.of(processGroups);
      return this;
    }
    
    public Builder appendProcessGroups(ImmutableList<ProcessGroupInfo> processGroups) {
      this.processGroups = Optional.of(this.processGroups.map(currentProcessGroups -> ImmutableList
          .<ProcessGroupInfo>builder()
          .addAll(currentProcessGroups)
          .addAll(processGroups)
          .build())
        .orElse(processGroups));
      return this;
    }
    
    public Builder withDebug(boolean debug) {
      this.debug = debug;
      return this;
    }
    
    public Config build() {
      Preconditions.checkArgument(dataDir != null, 
          "no database directory specified and environment variable PGDATA unset");
      
      return new Config(dataDir, interval, ppid, 
          prometheusService, prometheusBind, prometheusPort,
          noPrintHeader, showSystem, showOther, 
          processGroups, debug);
    }
  }
}
