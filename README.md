# pgio

A Java CLI turned in executable using GraalVM that is able to capture disk IO 
usage stats per process group (default groups are based on common postgresql 
installation) and total of the system.
The tool produce a stream of data that can be stored and interpreted (using 
CSV by default or exporting to Prometheus) to analyze and find out which part 
of PostgreSQL is using the disk most during a period of time.

## Stats collected

pgio runs only on Linux modern kernels (>= 2.6.x) reading `/proc/vmstat` and 
`/proc/<pid>/(cmdline|stat|io)` 
(see [here](http://man7.org/linux/man-pages/man5/proc.5.html))

Taking iotop as a reference (see [here](https://unix.stackexchange.com/questions/248197/iotop-showing-1-5-mb-s-of-disk-write-but-all-programs-have-0-00-b-s/248218#248218)):

iotop read information per process from `/proc/<pid>/io`, in particular:

```
rchar: characters read
     The number of bytes which this task has caused to be
     read from storage.  This is simply the sum of bytes
     which this process passed to read(2) and similar system
     calls.  It includes things such as terminal I/O and is
     unaffected by whether or not actual physical disk I/O
     was required (the read might have been satisfied from
     pagecache).

wchar: characters written
     The number of bytes which this task has caused, or
     shall cause to be written to disk.  Similar caveats
     apply here as with rchar.
```

iotop read information globally from /proc/vmstat, in particular:

```
pgpgin – Number of kilobytes the system has paged in from disk per second.
pgpgout – Number of kilobytes the system has paged out to disk per second.
```

Combining per process info with global info iotop calculate the % of write/read 
throughput each process is consuming.

Referenced iotop is a python rewrite of original [iotop](https://github.com/analogue/iotop).

Original [iotop code](https://github.com/Tomas-M/iotop) uses 
[Taskstats](https://www.kernel.org/doc/Documentation/accounting/taskstats.txt).
Seems that stats used from taskstats calls includes following stats from 
`/proc/<pid>/io`:


```
read_bytes: bytes read
     Attempt to count the number of bytes which this process
     really did cause to be fetched from the storage layer.
     This is accurate for block-backed filesystems.

write_bytes: bytes written
     Attempt to count the number of bytes which this process
     caused to be sent to the storage layer.

cancelled_write_bytes:
     The big inaccuracy here is truncate.  If a process
     writes 1MB to a file and then deletes the file, it will
     in fact perform no writeout.  But it will have been
     accounted as having caused 1MB of write.  In other
     words: this field represents the number of bytes which
     this process caused to not happen, by truncating page‐
     cache.  A task can cause "negative" I/O too.  If this
     task truncates some dirty pagecache, some I/O which
     another task has been accounted for (in its
     write_bytes) will not be happening.
```

Since relation between global and per process info can be out of sync we include
`rchar` and `wchar` (more correlated to pgpgio and pgpgout but that could 
include io stats not related to disk) stats together with `read_bytes`, 
`write_bytes` and `cancelled_write_bytes` (directly related to disk io but less 
precise) stats to allow a better analisys of the stats collected.

## Build

To build the project as a tar.gz with all Java dependencies:

```
mvn clean package -P assembler
```

To build the project as a tar.gz with an executable built using [GraalVM](https://www.graalvm.org/) 
`native-image` tool:

```
export GRAALVM_HOME=<path to graalvm home>
mvn clean package -P executable
```

## Run

This will generate output with collected stats of grouped processes (should be run 
as user with sufficient privileges like postgres) every 3 seconds:

```
bin/pgio -D <postgresql data dir>
```

## Options

```
Option                      Description                                        
------                      -----------                                        
-D <String>                 Specifies the file system location of the database 
                              configuration files. If this is omitted, the     
                              environment variable PGDATA is used.             
-a, --advanced              Enable advanced options                            
-d, --debug                 Show debug messages                                
-g, --group <String>        Group results using specified group configuration  
                              file (advanced option)                           
-h, --help                  Displays this help message and quit                
-i, --interval <String>     Interval in milliseconds to gather stats (default: 
                              3000)                                            
--no-print-header           Suppress print of CSV header                       
-o, --show-other            Print read/write data not accounted by any listed  
                              process                                          
--ppid <String>             Parent pid of the process to scan (advanced option)
--prometheus-bind <String>  The bind address of prometheus service (advanced   
                              option)                                          
--prometheus-port <String>  The port of prometheus service (advanced option)   
--prometheus-service        Run as a prometheus service (advanced option)      
-s, --show-system           Print read/write data for the whole system         
-v, --version               Show version and quit        
```

### Group configuration file

Specifying this file will allow to change default groups configuration.

A JSON indicating groups has the following syntax (regular expression will use 
pattern from [java.util.regex.Pattern](https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html):

```
[
 { "<group name>": [ <list of regexp> ] }
 ...
]
```

Default group configuration is:

```
[
  { "archiver": [ ".*archiver.*" ] },
  { "wal sender": [ ".*wal sender.*" ] },
  { "bgwriter": [ ".*writer process.*" ] },
  { "autovacuum": [ ".*autovacuum worker process.*" ] },
  { "stats": [ ".*stats collector process.*" ] },
  { "wal writer": [ ".*wal writer process.*" ] },
  { "checkpoint": [ ".*checkpointer process.*" ] },
  { "query": [ "postgres: " ] }
]
```

If configuration is save in file `postgresql.json` it can be used as follow:

```
bin/pgio -D <postgresql data dir> --advanced --group postgresql.json
```

The expected output will include following groups:

* archiver
* wal sender
* bgwriter
* autovacuum
* stats
* wal writer
* checkpoint
* query

The idea is that you can extend those groups to include other relevant 
tools like wal-e, pgbouncer, particual user's queries.

## Prometheus service

To start pgio as a prometheus service:

```
bin/pgio -D <postgresql data dir> --advanced --prometheus-service --prometheus-bind 0.0.0.0 --prometheus-port 9090
```

### CSV output example

```
$ pgio
timestamp,pid,ppid,label,rchar,wchar,read_bytes,write_bytes,cancelled_write_bytes
2018-12-19T14:42:11.070Z,"archiver",,,0,0,0,0,0
2018-12-19T14:42:11.070Z,"wal sender",,,0,0,0,0,0
2018-12-19T14:42:11.070Z,"bgwriter",,,0,0,0,0,0
2018-12-19T14:42:11.070Z,"autovacuum",,,0,0,0,0,0
2018-12-19T14:42:11.070Z,"stats",,,0,0,0,0,0
2018-12-19T14:42:11.070Z,"wal writer",,,0,0,0,0,0
2018-12-19T14:42:11.069Z,"checkpoint",,,0,0,0,0,0
2018-12-19T14:42:11.070Z,"other",,,0,0,0,0,0
2018-12-19T14:42:14.017Z,"archiver",,,0,0,0,0,0
2018-12-19T14:42:14.017Z,"wal sender",,,0,0,0,0,0
2018-12-19T14:42:14.016Z,"bgwriter",,,1,50061313,73728,50061312,0
2018-12-19T14:42:14.017Z,"autovacuum",,,0,0,0,0,0
2018-12-19T14:42:14.017Z,"stats",,,0,0,0,0,0
2018-12-19T14:42:14.016Z,"wal writer",,,0,50061312,73728,50061312,0
2018-12-19T14:42:14.016Z,"checkpoint",,,0,0,0,0,0
2018-12-19T14:42:14.017Z,"other",,,42409984,342833920,30908416,249819136,0
2018-12-19T14:42:17.017Z,"archiver",,,0,0,0,0,0
2018-12-19T14:42:17.017Z,"wal sender",,,0,0,0,0,0
2018-12-19T14:42:17.016Z,"bgwriter",,,0,75595776,135168,75595776,0
2018-12-19T14:42:17.017Z,"autovacuum",,,0,0,0,0,0
2018-12-19T14:42:17.016Z,"stats",,,0,0,0,0,0
2018-12-19T14:42:17.016Z,"wal writer",,,0,75595776,135168,75595776,0
2018-12-19T14:42:17.016Z,"checkpoint",,,0,0,0,0,0
2018-12-19T14:42:17.016Z,"other",,,44113920,359063552,45285376,244834304,0
2018-12-19T14:42:20.019Z,"archiver",,,0,0,0,0,0
2018-12-19T14:42:20.019Z,"wal sender",,,0,0,0,0,0
2018-12-19T14:42:20.019Z,"bgwriter",,,0,98852864,0,98852864,0
2018-12-19T14:42:20.020Z,"autovacuum",,,0,0,0,0,0
2018-12-19T14:42:20.019Z,"stats",,,0,0,0,0,0
2018-12-19T14:42:20.019Z,"wal writer",,,0,98852864,0,98852864,0
2018-12-19T14:42:20.018Z,"checkpoint",,,0,221370,299008,225280,0
2018-12-19T14:42:20.019Z,"other",,,44843008,335036416,45166592,235864064,0
2018-12-19T14:42:23.019Z,"archiver",,,0,0,0,0,0
2018-12-19T14:42:23.019Z,"wal sender",,,0,0,0,0,0
2018-12-19T14:42:23.018Z,"bgwriter",,,1,9003009,0,9003008,0
2018-12-19T14:42:23.020Z,"autovacuum",,,0,0,0,0,0
2018-12-19T14:42:23.019Z,"stats",,,0,0,4096,0,0
2018-12-19T14:42:23.018Z,"wal writer",,,1,9003009,0,9003008,0
2018-12-19T14:42:23.018Z,"checkpoint",,,1,16670992,409600,16678912,0
2018-12-19T14:42:23.019Z,"other",,,8633088,75464765,8630272,58880000,0
2018-12-19T14:42:26.023Z,"archiver",,,0,0,0,0,0
2018-12-19T14:42:26.023Z,"wal sender",,,0,0,0,0,0
2018-12-19T14:42:26.022Z,"bgwriter",,,0,0,0,0,0
2018-12-19T14:42:26.024Z,"autovacuum",,,0,0,0,0,0
2018-12-19T14:42:26.022Z,"stats",,,0,0,0,0,0
2018-12-19T14:42:26.022Z,"wal writer",,,0,0,0,0,0
2018-12-19T14:42:26.022Z,"checkpoint",,,0,0,0,0,0
2018-12-19T14:42:26.022Z,"other",,,0,0,0,0,0
```
